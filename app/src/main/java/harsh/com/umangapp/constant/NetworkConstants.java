package harsh.com.umangapp.constant;

/**
 * Created by Harsh on 9/28/2017.
 */

public interface NetworkConstants {
    public static final String NETWORK_IP = "https://vidyaroha.com/umang";
    public static final String LOGIN_URL = NETWORK_IP + "/applogin.php";
    public static final String FEED_URL = "https://api.androidhive.info/feed/feed.json";
    public static final String EVENT_IMAGE_URL = NETWORK_IP + "/images/";
}
