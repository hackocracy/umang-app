package harsh.com.umangapp.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Harsh on 9/27/2017.
 */

public class SessionManager {
    // Shared preferences file name
    private static final String PREF_NAME = "AndroidLogin";
    private static final String KEY_IS_LOGGEDIN = "isLoggedIn";
    public static final String LIVE_EVENTS = "LiveEvents";
    private static String TAG = SessionManager.class.getSimpleName();
    // Shared Preferences
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    Context _context;
    // Shared pref mode
    int PRIVATE_MODE = 0;

    public SessionManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public void setLogin(boolean isLoggedIn) {

        editor.putBoolean(KEY_IS_LOGGEDIN, isLoggedIn);

        // commit changes
        editor.commit();

        Log.d(TAG, "User login session modified!");
    }

    public boolean isLoggedIn() {
        return pref.getBoolean(KEY_IS_LOGGEDIN, false);
    }

    public void saveFavourite(Context context, List<String> eventId) {
        SharedPreferences settings;
        SharedPreferences.Editor editor;

        settings = context.getSharedPreferences(PREF_NAME,
                Context.MODE_PRIVATE);
        editor = settings.edit();

        Gson gson = new Gson();
        String jsonFavorites = gson.toJson(eventId);
        editor.putString(LIVE_EVENTS, jsonFavorites);
        editor.commit();
    }

    public void addEvent(Context context, String id) {
        List<String> liveEvents = getEvents(context);
        if (liveEvents == null)
            liveEvents = new ArrayList<>();
        liveEvents.add(id);
        saveFavourite(context, liveEvents);
    }

    public void removeEvent(Context context, String id) {
        ArrayList<String> liveEvents = getEvents(context);
        Iterator<String> iterator = liveEvents.iterator();
        while (iterator.hasNext()) {
            String id1 = iterator.next();
            if (id1.equals(id)) {
                iterator.remove();
                saveFavourite(context, liveEvents);
            }
        }
    }

    public ArrayList<String> getEvents(Context context) {
        SharedPreferences settings;
        List<String> liveEvents;

        settings = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);

        if (settings.contains(LIVE_EVENTS)) {
            String jsonFavourites = settings.getString(LIVE_EVENTS, null);
            Gson gson = new Gson();
            String[] favSongs = gson.fromJson(jsonFavourites, String[].class);

            liveEvents = Arrays.asList(favSongs);
            liveEvents = new ArrayList<String>(liveEvents);
        } else {
            return null;
        }
        return (ArrayList<String>) liveEvents;
    }
}
