package harsh.com.umangapp.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;
import harsh.com.umangapp.R;
import harsh.com.umangapp.app.AppController;
import harsh.com.umangapp.constant.NetworkConstants;
import harsh.com.umangapp.dto.EventDTO;
import harsh.com.umangapp.util.SessionManager;

public class EventActivity extends AppCompatActivity {

    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.eventImg)
    ImageView eventImg;
    @Bind(R.id.txtDesc)
    TextView eventDesc;
    @Bind(R.id.txtStartDate)
    TextView startDate;
    @Bind(R.id.txtEndDate)
    TextView endDate;
    @Bind(R.id.txtVenue)
    TextView eventVenue;
    @Bind(R.id.btnRegisterEvent)
    Button btnRegisterEvent;
    private SharedPreferences preferences;
    private String eventid;
    private String eventCode;
    private SessionManager sessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event);
        preferences = getSharedPreferences("AndroidLogin", Context.MODE_PRIVATE);
        sessionManager = new SessionManager(getApplicationContext());
        populate();
    }

    private void populate() {
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Events");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        if (preferences.getBoolean("user_profile", false)) {
            btnRegisterEvent.setBackgroundResource(R.drawable.btn_background);
            btnRegisterEvent.setText("Register");
        }
        Bundle bundle = getIntent().getBundleExtra("data");
        if (null != bundle) {
            if (bundle.containsKey("event")) {
                EventDTO dto = (EventDTO) bundle.getSerializable("event");
                assert dto != null;
                if (dto.getCode().equals("1")) {
                    btnRegisterEvent.setBackgroundColor(getResources().getColor(R.color.green));
                    btnRegisterEvent.setText("Attend Event");
                }
                Glide.with(this)
                        .load(dto.getEventImageUrl())
                        .centerCrop()
                        .placeholder(R.mipmap.profile_cover)
                        .crossFade()
                        .into(eventImg);
                eventDesc.setText(dto.getEventDesc());
                startDate.setText(dto.getStartDate());
                endDate.setText(dto.getEndDate());
                eventVenue.setText(dto.getVenue());
                eventid = dto.getEventId();
                eventCode = dto.getCode();
            }
        }

        if (checkEvent(eventid)) {
            btnRegisterEvent.setBackgroundColor(getResources().getColor(R.color.red));
            btnRegisterEvent.setText("Sign Out");
        }

        btnRegisterEvent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (checkEvent(eventid)) {
                    signOut();
                    return;
                }
                if (!preferences.getBoolean("user_profile", false)) {
                    Toast.makeText(EventActivity.this, "Mandatory Profile Details Required!!!", Toast.LENGTH_LONG).show();
                    startActivity(new Intent(EventActivity.this, ProfileActivity.class));
                    finish();
                } else if (eventCode.equals("1")) {
                    Intent intent = new Intent(EventActivity.this, BarcodeActivity.class);
                    preferences.edit().putString("eventid", eventid).apply();
                    startActivity(intent);
                } else {
                    registerEvent();
                }
            }
        });
    }

    private void signOut() {
        final MaterialDialog dialog = new MaterialDialog.Builder(EventActivity.this)
                .title("Loading")
                .content("Please Wait")
                .progress(true, 0)
                .show();
        StringRequest request = new StringRequest(Request.Method.POST, NetworkConstants.LOGIN_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.i("Response::", response);
                dialog.dismiss();
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    int code = jsonObject.getInt("code");
                    if (code == 1) {
                        btnRegisterEvent.setBackgroundResource(R.drawable.btn_background);
                        btnRegisterEvent.setText("Register");
                        sessionManager.removeEvent(getApplicationContext(), eventid);
                        Toast.makeText(EventActivity.this, "Logged Out Successfully!!!", Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(EventActivity.this, DashboardActivity.class));
                        finish();
                    } else {
                        Toast.makeText(EventActivity.this, "Some Error Occurred!!!", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                } catch (JSONException e) {
                    dialog.dismiss();
                    e.printStackTrace();
                    Toast.makeText(EventActivity.this, "Some Error Occurred!!!", Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("Error::", error.toString());
                dialog.dismiss();
                Toast.makeText(EventActivity.this, "Some Error Occurred!!! Scan Again", Toast.LENGTH_SHORT).show();
                finish();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Calendar c = Calendar.getInstance();
                System.out.println("Current time =&gt; " + c.getTime());
                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String formattedDate = df.format(c.getTime());
                Map<String, String> params = new HashMap<>();
                params.put("useremailid", preferences.getString("user_email", ""));
                params.put("atteventid", preferences.getString("eventid", ""));
                params.put("attstop", formattedDate);
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(request);
    }

    private void registerEvent() {
        final MaterialDialog dialog = new MaterialDialog.Builder(EventActivity.this)
                .title("Loading")
                .content("Please Wait")
                .progress(true, 0)
                .show();
        StringRequest request = new StringRequest(Request.Method.POST, NetworkConstants.LOGIN_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.i("Response:::", response);
                dialog.dismiss();
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    int code = jsonObject.getInt("code");
                    if (code == 1) {
                        new MaterialDialog.Builder(EventActivity.this)
                                .title("Success")
                                .content("You have successfully registered in this event. You can view it on My Events screen.")
                                .positiveText("Ok")
                                .onPositive(new MaterialDialog.SingleButtonCallback() {
                                    @Override
                                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                        startActivity(new Intent(EventActivity.this, DashboardActivity.class));
                                        finish();
                                    }
                                })
                                .show();
                    } else {
                        Toast.makeText(EventActivity.this, "Some Error Occurred!!!", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(EventActivity.this, "Some Error Occurred!!!", Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Error::", error.toString());
                dialog.dismiss();
                Toast.makeText(EventActivity.this, "Some Error Occurred!!!", Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("register", preferences.getString("user_email", ""));
                params.put("eventid", eventid);
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(request);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private boolean checkEvent(String currentEvent) {
        boolean isFav = false;
        List<String> listEvents = sessionManager.getEvents(EventActivity.this);
        try {
            if (listEvents != null) {
                for (String event : listEvents) {
                    if (event.equals(currentEvent)) {
                        isFav = true;
                        break;
                    }
                }
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        return isFav;
    }
}
