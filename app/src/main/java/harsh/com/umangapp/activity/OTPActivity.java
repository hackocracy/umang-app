package harsh.com.umangapp.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import harsh.com.umangapp.R;
import harsh.com.umangapp.app.AppController;
import harsh.com.umangapp.constant.NetworkConstants;
import harsh.com.umangapp.util.NetworkCheck;
import harsh.com.umangapp.util.SessionManager;

public class OTPActivity extends AppCompatActivity {

    @Bind(R.id.etOtp1)
    EditText etOtp1;
    @Bind(R.id.etOtp2)
    EditText etOtp2;
    @Bind(R.id.etOtp3)
    EditText etOtp3;
    @Bind(R.id.etOtp4)
    EditText etOtp4;
    @Bind(R.id.txtTimer)
    TextView txtTimer;
    @Bind(R.id.txtResend)
    TextView txtResendOTP;
    @Bind(R.id.btnSubmitOtp)
    Button btnSubmitOtp;
    SharedPreferences preferences;
    SessionManager sessionManager;

    @OnClick(R.id.btnSubmitOtp)
    void dashboard() {
        if (!NetworkCheck.isNetworkAvailable(getApplicationContext())) {
            Toast.makeText(OTPActivity.this, "No Internet Connection!!!", Toast.LENGTH_SHORT).show();
            return;
        }
        String OTP = etOtp1.getText().toString()
                + etOtp2.getText().toString()
                + etOtp3.getText().toString()
                + etOtp4.getText().toString();
        confirmOTP(OTP);
    }

    @OnClick(R.id.txtResend)
    void resendOTP() {
        txtTimer.setVisibility(View.VISIBLE);
        startTimer();
        sendOtp();
    }

    private void confirmOTP(final String otp) {
        if (!NetworkCheck.isNetworkAvailable(OTPActivity.this)) {
            Toast.makeText(OTPActivity.this, "No Internet!!!", Toast.LENGTH_SHORT).show();
            return;
        }
        final MaterialDialog dialog = new MaterialDialog.Builder(this)
                .title("Loading")
                .content("Please Wait...")
                .progress(true, 0)
                .cancelable(false)
                .show();
        StringRequest request = new StringRequest(Request.Method.POST, NetworkConstants.LOGIN_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.i("OTP Response", response);
                dialog.dismiss();
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    int code = jsonObject.getInt("code");
                    if (code == 1) {
                        Toast.makeText(OTPActivity.this, "Authenticated.", Toast.LENGTH_SHORT).show();
                        sessionManager.setLogin(true);
                        startActivity(new Intent(OTPActivity.this, DashboardActivity.class));
                        finish();
                    } else {
                        Toast.makeText(OTPActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Error::", error.toString());
                Toast.makeText(OTPActivity.this, "Some Error Occurred!!!", Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("phn", preferences.getString("user_email", null));
                params.put("otp", otp);
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(request);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp);
        preferences = getSharedPreferences("AndroidLogin", Context.MODE_PRIVATE);
        sessionManager = new SessionManager(getApplicationContext());
        populate();
    }

    private void populate() {
        ButterKnife.bind(this);

        sendOtp();

        etOtp1.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (etOtp1.getText().toString().length() == 1)     //size as per your requirement
                {
                    etOtp2.requestFocus();
                }
            }

            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            public void afterTextChanged(Editable s) {
            }

        });


        etOtp2.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (etOtp2.getText().toString().length() == 1)     //size as per your requirement
                {
                    etOtp3.requestFocus();
                }
            }

            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            public void afterTextChanged(Editable s) {
            }

        });


        etOtp3.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (etOtp3.getText().toString().length() == 1)     //size as per your requirement
                {
                    etOtp4.requestFocus();
                }
            }

            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            public void afterTextChanged(Editable s) {
            }

        });


        etOtp4.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (etOtp4.getText().toString().length() == 1)     //size as per your requirement
                {
                    btnSubmitOtp.setEnabled(true);
                    btnSubmitOtp.setBackgroundColor(getResources().getColor(R.color.green));
                }
                if (etOtp4.getText().toString().length() == 0)     //size as per your requirement
                {
                    btnSubmitOtp.setEnabled(false);
                    btnSubmitOtp.setBackgroundColor(Color.GRAY);
                }
            }

            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            public void afterTextChanged(Editable s) {
            }

        });
    }

    private void sendOtp() {
        if (!NetworkCheck.isNetworkAvailable(OTPActivity.this)) {
            Toast.makeText(OTPActivity.this, "No Internet!!!", Toast.LENGTH_SHORT).show();
            return;
        }
        final MaterialDialog dialog = new MaterialDialog.Builder(this)
                .title("Loading")
                .content("Please Wait...")
                .progress(true, 0)
                .cancelable(false)
                .show();
        StringRequest request = new StringRequest(Request.Method.POST, NetworkConstants.LOGIN_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.i("OTP Response", response);
                dialog.dismiss();
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    int code = jsonObject.getInt("code");
                    if (code == 1) {
                        Toast.makeText(OTPActivity.this, "Check Your Email for OTP.", Toast.LENGTH_SHORT).show();
                        startTimer();
                    } else {
                        Toast.makeText(OTPActivity.this, "Some Error Occurred!!!", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Error::", error.toString());
                Toast.makeText(OTPActivity.this, "Some Error Occurred!!!", Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("loginid", preferences.getString("user_email", null));
                params.put("type", "u");
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(request);
    }

    private void startTimer() {
        new CountDownTimer(300000, 1000) {

            public void onTick(long millisUntilFinished) {
                txtTimer.setText("" + String.format("0%d : %d",
                        TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished),
                        TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) -
                                TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))));
            }

            public void onFinish() {
                txtTimer.setVisibility(View.INVISIBLE);
                txtResendOTP.setTextColor(getResources().getColor(R.color.com_facebook_blue));
                txtResendOTP.setEnabled(true);
            }
        }.start();
    }

    @Override
    public void onBackPressed() {
        moveTaskToBack(true);
    }
}
