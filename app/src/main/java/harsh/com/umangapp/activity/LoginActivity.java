package harsh.com.umangapp.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import harsh.com.umangapp.R;
import harsh.com.umangapp.util.NetworkCheck;

public class LoginActivity extends AppCompatActivity {

    private static final int RC_SIGN_IN = 9001;
    public GoogleApiClient mGoogleApiClient;
    @Bind(R.id.etEmail)
    EditText etEmail;
    @Bind(R.id.etName)
    EditText etName;
    @Bind(R.id.google_sign_in_button)
    SignInButton mGoogleSignInButton;
    @Bind(R.id.facebook_login_button)
    LoginButton mFacebookLoginButton;
    @Bind(R.id.btnSignIn)
    Button btnSignIn;
    @Bind(R.id.rlLogin)
    RelativeLayout rlLogin;
    @Bind(R.id.progressBar)
    ProgressBar progressBar;
    String email = "", name = "", birthday = "";
    private SharedPreferences preferences;
    private CallbackManager mCallbackManager;

    @OnClick(R.id.btnSignIn)
    void signin() {
        if (!NetworkCheck.isNetworkAvailable(this)) {
            Snackbar.make(rlLogin, "No Internet Connection!!!", 2000).show();
            return;
        }
        if (etName.getText().toString().equals("")) {
            Snackbar.make(rlLogin, "Please enter your Name!!!", 2000).show();
            return;
        }
        if (etName.getText().toString().length() <= 3) {
            Snackbar.make(rlLogin, "Please enter a valid Name!!!", 2000).show();
            return;
        }
        if (etEmail.getText().toString().equals("")) {
            Snackbar.make(rlLogin, "Please enter an Email!!!", 2000).show();
            return;
        }
        if (!android.util.Patterns.EMAIL_ADDRESS.matcher(etEmail.getText().toString()).matches()) {
            Snackbar.make(rlLogin, "Please enter a valid Email!!!", 2000).show();
            return;
        }
        rlLogin.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
        name = etName.getText().toString();
        email = etEmail.getText().toString();
        updateUI();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_login);
        preferences = getSharedPreferences("AndroidLogin", Context.MODE_PRIVATE);
        populate();
    }

    private void populate() {
        ButterKnife.bind(this);
        progressBar.setVisibility(View.GONE);
        rlLogin.setVisibility(View.VISIBLE);

        /*Facebook Login*/
        mCallbackManager = CallbackManager.Factory.create();
        mFacebookLoginButton.setReadPermissions(Arrays.asList("public_profile", "email", "user_birthday"));
        mFacebookLoginButton.registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        Log.v("LoginActivity", response.toString());
                        try {
                            name = object.getString("name");
                            email = object.getString("email");
                            //birthday = object.getString("birthday");
                            updateUI();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,email,gender,birthday");
                request.setParameters(parameters);
                request.executeAsync();
                //updateUI();
            }

            @Override
            public void onCancel() {
                handleSignInResult();
            }

            @Override
            public void onError(FacebookException error) {
                Log.d(LoginActivity.class.getCanonicalName(), error.getMessage());
                handleSignInResult();
            }
        });


        /*Google Login*/
        mGoogleSignInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (NetworkCheck.isNetworkAvailable(getApplicationContext())) {
                    rlLogin.setVisibility(View.GONE);
                    progressBar.setVisibility(View.VISIBLE);
                    signInWithGoogle();
                } else {
                    Snackbar.make(rlLogin, "No Internet Connection!!!", 2000).show();
                }
            }
        });
    }

    private void handleSignInResult() {
        if (!NetworkCheck.isNetworkAvailable(getApplicationContext())) {
            Snackbar.make(rlLogin, "No Internet Connection!!!", 2000).show();
        }
    }

    private void signInWithGoogle() {
        if (mGoogleApiClient != null) {
            mGoogleApiClient.disconnect();
        }

        GoogleSignInOptions googleSignInOptions = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, googleSignInOptions)
                .build();

        final Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mCallbackManager.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            if (result.isSuccess()) {
                GoogleSignInAccount signInAccount = result.getSignInAccount();
                assert signInAccount != null;
                name = signInAccount.getDisplayName();
                email = signInAccount.getEmail();
                Log.i("GoogleResult", name + " " + email);
                updateUI();
            }
        }
    }

    private void updateUI() {
        preferences.edit().putString("user_name", name).apply();
        preferences.edit().putString("user_email", email).apply();
        preferences.edit().putBoolean("user_profile", false).apply();
        //preferences.edit().putString("user_birthday", birthday).apply();
        startActivity(new Intent(LoginActivity.this, OTPActivity.class));
        finish();
    }
}
