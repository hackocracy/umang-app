package harsh.com.umangapp.activity;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import harsh.com.umangapp.R;
import harsh.com.umangapp.app.AppController;
import harsh.com.umangapp.constant.NetworkConstants;
import harsh.com.umangapp.util.NetworkCheck;

public class ProfileActivity extends AppCompatActivity {

    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.etEmailHome)
    EditText etEmailHome;
    @Bind(R.id.etContactHome)
    EditText etContact;
    @Bind(R.id.txtDob)
    TextView txtDob;
    @Bind(R.id.radioGroupGender)
    RadioGroup genderGroup;
    @Bind(R.id.rdbtnMale)
    RadioButton male;
    @Bind(R.id.rdbtnFemale)
    RadioButton female;
    @Bind(R.id.rdbtnOther)
    RadioButton other;
    @Bind(R.id.fab)
    FloatingActionButton fab;
    @Bind(R.id.etHomeAddress)
    EditText etHomeAddress;
    @Bind(R.id.coordinator)
    CoordinatorLayout coordinatorLayout;
    private SharedPreferences preferences;
    private String gender;

    @OnClick(R.id.txtDob)
    void calendar() {
        Calendar calendar = Calendar.getInstance();
        DatePickerDialog.OnDateSetListener dateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                int s = i1 + 1;
                String a = i2 + "-" + s + "-" + i;
                txtDob.setText("" + a);
            }
        };
        DatePickerDialog d = new DatePickerDialog(this, dateSetListener, calendar.get(Calendar.YEAR)
                , calendar.get(Calendar.MONTH)
                , calendar.get(Calendar.DATE));
        d.show();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        preferences = getSharedPreferences("AndroidLogin", Context.MODE_PRIVATE);
        populate();
    }

    private void populate() {
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(preferences.getString("user_name", null));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etContact.requestFocus();
            }
        });
        etEmailHome.setText(preferences.getString("user_email", null));
        if (preferences.getBoolean("user_profile", false)) {
            etContact.setText(preferences.getString("user_contact", null));
            txtDob.setText(preferences.getString("user_dob", null));
            etHomeAddress.setText(preferences.getString("user_address", ""));
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_profile, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
            case R.id.action_save_profile:
                saveProfile();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void saveProfile() {
        if (!NetworkCheck.isNetworkAvailable(this)) {
            Toast.makeText(this, "No Internet Connection!!!", Toast.LENGTH_SHORT).show();
            return;
        }
        if (etContact.getText().toString().equals("")) {
            Toast.makeText(this, "Phone Number is Mandatory!!!", Toast.LENGTH_SHORT).show();
            return;
        }
        if (etContact.getText().toString().length() != 10) {
            Toast.makeText(this, "Invalid Phone Number!!!", Toast.LENGTH_SHORT).show();
            return;
        }
        if (etEmailHome.getText().toString().equals("")) {
            Toast.makeText(this, "Email Address is Mandatory!!!", Toast.LENGTH_SHORT).show();
            return;
        }
        if (!android.util.Patterns.EMAIL_ADDRESS.matcher(etEmailHome.getText().toString()).matches()) {
            Toast.makeText(this, "Invalid Email Address!!!", Toast.LENGTH_SHORT).show();
            return;
        }
        if (txtDob.getText().toString().equals(getString(R.string.date_of_birth))) {
            Toast.makeText(this, "Date of Birth is Mandatory!!!", Toast.LENGTH_SHORT).show();
            return;
        }
        if (male.isChecked()) {
            gender = "M";
        }
        if (female.isChecked()) {
            gender = "F";
        }
        if (other.isChecked()) {
            gender = "O";
        }
        if (!etHomeAddress.getText().toString().equals("")) {
            preferences.edit().putString("user_address", etHomeAddress.getText().toString()).apply();
        }
        preferences.edit().putString("user_gender", gender).apply();
        preferences.edit().putString("user_dob", txtDob.getText().toString()).apply();
        preferences.edit().putString("user_contact", etContact.getText().toString()).apply();
        preferences.edit().putString("user_email", etEmailHome.getText().toString()).apply();
        preferences.edit().putBoolean("user_profile", true).apply();

        final MaterialDialog dialog = new MaterialDialog.Builder(ProfileActivity.this)
                .title("Loading")
                .content("Please Wait")
                .progress(true, 0)
                .cancelable(false)
                .show();
        StringRequest request = new StringRequest(Request.Method.POST, NetworkConstants.LOGIN_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.i("Response:::", response);
                dialog.dismiss();
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    int code = jsonObject.getInt("code");
                    if (code == 1) {
                        Toast.makeText(ProfileActivity.this, "Profile Updated.", Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(ProfileActivity.this, DashboardActivity.class));
                        finish();
                    } else {
                        Toast.makeText(ProfileActivity.this, "Some Error Occurred!!!", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(ProfileActivity.this, "Some Error Occurred!!!", Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
                Log.e("Error::", error.toString());
                Toast.makeText(ProfileActivity.this, "Some Error Occurred!!!", Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("useremail", etEmailHome.getText().toString());
                params.put("name", preferences.getString("user_name", null));
                params.put("gender", gender);
                params.put("birthday", txtDob.getText().toString());
                params.put("phone", etContact.getText().toString());
                params.put("address", etHomeAddress.getText().toString());
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(request);
    }
}
