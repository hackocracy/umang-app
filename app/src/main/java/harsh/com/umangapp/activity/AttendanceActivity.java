package harsh.com.umangapp.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;
import harsh.com.umangapp.R;
import harsh.com.umangapp.app.AppController;
import harsh.com.umangapp.constant.NetworkConstants;
import harsh.com.umangapp.util.NetworkCheck;
import harsh.com.umangapp.util.SessionManager;

public class AttendanceActivity extends AppCompatActivity {

    @Bind(R.id.txtBarcodeValue)
    TextView txtBarcodeValue;
    @Bind(R.id.imgSuccess)
    ImageView imgQRScan;
    private SharedPreferences preferences;
    private SessionManager sessionManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_attendance);
        preferences = getSharedPreferences("AndroidLogin", Context.MODE_PRIVATE);
        populate();
    }

    private void populate() {
        ButterKnife.bind(this);
        sessionManager = new SessionManager(getApplicationContext());
        String barcode = getIntent().getStringExtra("code");

        // close the activity in case of empty barcode
        if (TextUtils.isEmpty(barcode)) {
            Toast.makeText(getApplicationContext(), "Barcode is empty!", Toast.LENGTH_LONG).show();
            finish();
        }
        if (!barcode.equals("umangeventattendance")) {
            imgQRScan.setImageResource(R.drawable.cancel);
            txtBarcodeValue.setText("Incorrect QR-Code scanned! Go back and scan again");
            return;
        }

        if (!NetworkCheck.isNetworkAvailable(getApplicationContext())) {
            Toast.makeText(AttendanceActivity.this, "No Internet!!!", Toast.LENGTH_SHORT).show();
            return;
        }

        final MaterialDialog dialog = new MaterialDialog.Builder(AttendanceActivity.this)
                .title("Loading")
                .content("Please Wait")
                .progress(true, 0)
                .show();
        StringRequest request = new StringRequest(Request.Method.POST, NetworkConstants.LOGIN_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.i("Response::", response);
                dialog.dismiss();
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    int code = jsonObject.getInt("code");
                    if (code == 1) {
                        imgQRScan.setImageResource(R.drawable.success);
                        txtBarcodeValue.setText("You are live in the event. Yayy!!!");
                        sessionManager.addEvent(getApplicationContext(), preferences.getString("eventid", null));
                    } else {
                        Toast.makeText(AttendanceActivity.this, "Some Error Occurred!!! Scan Again", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                } catch (JSONException e) {
                    dialog.dismiss();
                    e.printStackTrace();
                    Toast.makeText(AttendanceActivity.this, "Some Error Occurred!!! Scan Again", Toast.LENGTH_SHORT).show();
                    finish();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("Error::", error.toString());
                dialog.dismiss();
                Toast.makeText(AttendanceActivity.this, "Some Error Occurred!!! Scan Again", Toast.LENGTH_SHORT).show();
                finish();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Calendar c = Calendar.getInstance();
                System.out.println("Current time =&gt; " + c.getTime());
                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String formattedDate = df.format(c.getTime());
                Map<String, String> params = new HashMap<>();
                params.put("useremailid", preferences.getString("user_email", ""));
                params.put("atteventid", preferences.getString("eventid", ""));
                params.put("attstart", formattedDate);
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(request);
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(this, DashboardActivity.class));
        finish();
    }
}
