package harsh.com.umangapp.activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;

import butterknife.Bind;
import butterknife.ButterKnife;
import harsh.com.umangapp.R;
import harsh.com.umangapp.fragment.ContactUsFragment;
import harsh.com.umangapp.fragment.FeedbackFragment;
import harsh.com.umangapp.fragment.HomeFragment;
import harsh.com.umangapp.fragment.MediaMentionsFragment;
import harsh.com.umangapp.fragment.MyEventsFragment;
import harsh.com.umangapp.fragment.PhotoGalleryFragment;
import harsh.com.umangapp.util.NetworkCheck;
import harsh.com.umangapp.util.SessionManager;

public class DashboardActivity extends AppCompatActivity {

    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.drawer_layout)
    DrawerLayout drawer_layout;
    @Bind(R.id.navigation_view)
    NavigationView navigation_view;
    private SharedPreferences preferences;
    private MenuItem previousMenuItem;
    private GoogleApiClient mGoogleApiClient;
    private SessionManager sessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_dashboard);
        preferences = getSharedPreferences("AndroidLogin", Context.MODE_PRIVATE);
        populate();
    }

    private void populate() {
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getString(R.string.home));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ActivityCompat.requestPermissions(DashboardActivity.this, new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 100);
        sessionManager = new SessionManager(getApplicationContext());

        navigation_view.inflateMenu(R.menu.menu_drawer);
        navigation_view.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem item) {
                if (previousMenuItem != null)
                    previousMenuItem.setChecked(false);

                item.setCheckable(true);
                item.setChecked(true);

                previousMenuItem = item;

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        drawer_layout.closeDrawers();
                    }
                }, 200);
                //opening the fragments upon click of every item in navigtion drawer
                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                switch (item.getItemId()) {
                    case R.id.home:
                        HomeFragment homeFragment = new HomeFragment();
                        fragmentTransaction.replace(R.id.frame, homeFragment);
                        fragmentTransaction.commit();
                        getSupportActionBar().show();
                        getSupportActionBar().setTitle(getString(R.string.home));
                        return true;
                    case R.id.my_events:
                        MyEventsFragment myEventsFragment = new MyEventsFragment();
                        fragmentTransaction.replace(R.id.frame, myEventsFragment);
                        fragmentTransaction.commit();
                        getSupportActionBar().show();
                        getSupportActionBar().setTitle(getString(R.string.my_events));
                        return true;
                    case R.id.media:
                        MediaMentionsFragment mediaMentionsFragment = new MediaMentionsFragment();
                        fragmentTransaction.replace(R.id.frame, mediaMentionsFragment);
                        fragmentTransaction.commit();
                        getSupportActionBar().show();
                        getSupportActionBar().setTitle(getString(R.string.media_mentions));
                        return true;
                    case R.id.gallery:
                        PhotoGalleryFragment photoGalleryFragment = new PhotoGalleryFragment();
                        fragmentTransaction.replace(R.id.frame, photoGalleryFragment);
                        fragmentTransaction.commit();
                        getSupportActionBar().show();
                        getSupportActionBar().setTitle(getString(R.string.gallery));
                        return true;
                    case R.id.achievements:
                        startActivity(new Intent(DashboardActivity.this, BarcodeActivity.class));
                        return true;
                    case R.id.feedback:
                        FeedbackFragment feedbackFragment = new FeedbackFragment();
                        fragmentTransaction.replace(R.id.frame, feedbackFragment);
                        fragmentTransaction.commit();
                        getSupportActionBar().show();
                        getSupportActionBar().setTitle(getString(R.string.feedback));
                        return true;
                    case R.id.contact_us:
                        ContactUsFragment contactUsFragment = new ContactUsFragment();
                        fragmentTransaction.replace(R.id.frame, contactUsFragment);
                        fragmentTransaction.commit();
                        getSupportActionBar().hide();
                        return true;
                    case R.id.logout:
                        if (NetworkCheck.isNetworkAvailable(getApplicationContext())) {
                            new MaterialDialog.Builder(DashboardActivity.this)
                                    .title("Log out")
                                    .content("Do you really want to logout?")
                                    .positiveText("Yes")
                                    .negativeText("No")
                                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                                        @Override
                                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                            logout();
                                        }
                                    })
                                    .onNegative(new MaterialDialog.SingleButtonCallback() {
                                        @Override
                                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                            dialog.dismiss();
                                        }
                                    })
                                    .show();
                            return true;
                        } else {
                            Snackbar.make(drawer_layout, "No internet Connction!!!", 2000).show();
                            return true;
                        }
                }
                return true;
            }
        });

        //inflating the dashboard fragment
        HomeFragment fragment = new HomeFragment();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frame, fragment);
        fragmentTransaction.commit();

        //syncing the drawer toggle
        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawer_layout, R.string.openDrawer, R.string.closeDrawer) {
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }
        };

        drawer_layout.setDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();

        View convertView = LayoutInflater.from(this).inflate(R.layout.drawer_header, navigation_view, false);
        navigation_view.addHeaderView(convertView);
        TextView userName = convertView.findViewById(R.id.txtUserName);
        TextView userEmail = convertView.findViewById(R.id.txtUserEmail);
        userName.setText(preferences.getString("user_name", null));
        userEmail.setText(preferences.getString("user_email", null));
    }

    @Override
    protected void onStart() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
        mGoogleApiClient.connect();
        super.onStart();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case R.id.action_profile:
                startActivity(new Intent(this, ProfileActivity.class));
                break;
            case android.R.id.home:
                drawer_layout.openDrawer(GravityCompat.START);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void logout() {
        sessionManager.setLogin(false);
        preferences.edit().clear().apply();
        /*Facebook Logout*/
        LoginManager.getInstance().logOut();
        /*Google Logout*/
        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(new ResultCallback<Status>() {
            @Override
            public void onResult(@NonNull Status status) {
                startActivity(new Intent(DashboardActivity.this, LoginActivity.class));
                ActivityCompat.finishAffinity(DashboardActivity.this);
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case 100:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {

                } else {
                    Toast.makeText(this, "Permissions are Required!!!", Toast.LENGTH_SHORT).show();
                }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onBackPressed() {
        Fragment f = getSupportFragmentManager().findFragmentById(R.id.frame);
        if (f instanceof HomeFragment)
            super.onBackPressed();
        else {
            Intent intent = new Intent(DashboardActivity.this, DashboardActivity.class);
            startActivity(intent);
            finish();
        }
    }
}
