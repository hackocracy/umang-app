package harsh.com.umangapp.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import harsh.com.umangapp.R;
import harsh.com.umangapp.dto.MediaDTO;

/**
 * Created by Harsh on 9/28/2017.
 */

public class MediaAdapter extends RecyclerView.Adapter<MediaAdapter.MediaViewHolder> {

    private Context context;
    private List<MediaDTO> mediaDTOList;
    private LayoutInflater inflater;
    private OnItemClickListener listener;

    public MediaAdapter(Context context, List<MediaDTO> mediaDTOList, OnItemClickListener listener) {
        this.context = context;
        this.mediaDTOList = mediaDTOList;
        this.listener = listener;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public MediaViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.media_mentions_custom_row, parent, false);
        MediaViewHolder holder = new MediaViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(MediaViewHolder holder, int position) {
        holder.bind(mediaDTOList.get(position), listener);
    }

    @Override
    public int getItemCount() {
        return mediaDTOList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    /*Interface to handle click events of the Recycler view*/
    public interface OnItemClickListener {
        void onItemClick(MediaDTO mediaDTO);
    }

    class MediaViewHolder extends RecyclerView.ViewHolder {
        ImageView labelImage, mediaImage;
        TextView labelText, link;

        public MediaViewHolder(View itemView) {
            super(itemView);
            labelImage = itemView.findViewById(R.id.imgLabel);
            mediaImage = itemView.findViewById(R.id.imgMediaImage);
            labelText = itemView.findViewById(R.id.txtLabel);
            link = itemView.findViewById(R.id.txtLink);
        }

        public void bind(final MediaDTO mediaDTO, final OnItemClickListener listener) {
            Glide.with(context)
                    .load(mediaDTO.getLabelImage())
                    .centerCrop()
                    .placeholder(R.drawable.ic_action_user_gray)
                    .crossFade()
                    .into(labelImage);
            Glide.with(context)
                    .load(mediaDTO.getMediaImage())
                    .centerCrop()
                    .placeholder(R.drawable.event)
                    .crossFade()
                    .into(mediaImage);
            labelText.setText(mediaDTO.getLabel());
            link.setText(mediaDTO.getLink());
            link.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onItemClick(mediaDTO);
                }
            });
        }
    }
}
