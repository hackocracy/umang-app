package harsh.com.umangapp.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import harsh.com.umangapp.R;
import harsh.com.umangapp.dto.EventDTO;

/**
 * Created by Harsh on 9/28/2017.
 */

public class EventsAdapter extends RecyclerView.Adapter<EventsAdapter.EventViewHolder> {

    private Context context;
    private List<EventDTO> eventDTOs;
    private LayoutInflater inflater;
    private OnItemClickListener listener;

    public EventsAdapter(Context context, List<EventDTO> eventDTOs, OnItemClickListener listener) {
        this.context = context;
        this.eventDTOs = eventDTOs;
        this.listener = listener;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public EventViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.events_custom_row, parent, false);
        EventViewHolder holder = new EventViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(EventViewHolder holder, int position) {
        holder.bind(eventDTOs.get(position), listener);
    }

    @Override
    public int getItemCount() {
        return eventDTOs.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    /*Interface to handle click events of the Recycler view*/
    public interface OnItemClickListener {
        void onItemClick(EventDTO eventDTO);
    }

    class EventViewHolder extends RecyclerView.ViewHolder {
        TextView eventName;
        ImageView eventImage;

        public EventViewHolder(View itemView) {
            super(itemView);
            eventImage = itemView.findViewById(R.id.imgEvent);
            eventName = itemView.findViewById(R.id.txtEventName);
        }

        public void bind(final EventDTO eventDTO, final OnItemClickListener listener) {
            Glide.with(context)
                    .load(eventDTO.getEventImageUrl())
                    .centerCrop()
                    .placeholder(R.drawable.event)
                    .crossFade()
                    .into(eventImage);
            eventName.setText(eventDTO.getEventName());
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onItemClick(eventDTO);
                }
            });
        }
    }
}
