package harsh.com.umangapp.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ThumbnailUtils;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import harsh.com.umangapp.R;
import harsh.com.umangapp.dto.GalleryDTO;

/**
 * Created by Harsh on 9/28/2017.
 */

public class GalleryAdapter extends RecyclerView.Adapter<GalleryAdapter.GalleryViewHolder> {

    private Context context;
    private List<GalleryDTO> galleryDTOList;
    private LayoutInflater inflater;
    private OnItemClickListener listener;

    public GalleryAdapter(Context context, List<GalleryDTO> galleryDTOList, OnItemClickListener listener) {
        this.context = context;
        this.galleryDTOList = galleryDTOList;
        this.listener = listener;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public GalleryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.events_custom_row, parent, false);
        GalleryViewHolder holder = new GalleryViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(GalleryViewHolder holder, int position) {
        holder.bind(galleryDTOList.get(position), listener);
    }

    @Override
    public int getItemCount() {
        return galleryDTOList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    /*Interface to handle click events of the Recycler view*/
    public interface OnItemClickListener {
        void onItemClick(GalleryDTO galleryDTO);
    }

    class GalleryViewHolder extends RecyclerView.ViewHolder {
        ImageView image;
        TextView imageName;

        public GalleryViewHolder(View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.imgEvent);
            imageName = itemView.findViewById(R.id.txtEventName);
        }

        public void bind(final GalleryDTO galleryDTO, final OnItemClickListener listener) {
            /*Glide.with(context)
                    .load(galleryDTO.getFilepath())
                    .centerCrop()
                    .placeholder(R.mipmap.ic_launcher)
                    .crossFade()
                    .into(image);*/
            Bitmap bitmap = BitmapFactory.decodeFile(galleryDTO.getFilepath());
            Bitmap resized = ThumbnailUtils.extractThumbnail(bitmap, bitmap.getWidth() / 2, bitmap.getHeight() / 2);
            image.setImageBitmap(resized);
            imageName.setText(galleryDTO.getFilename());
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onItemClick(galleryDTO);
                }
            });
        }
    }
}
