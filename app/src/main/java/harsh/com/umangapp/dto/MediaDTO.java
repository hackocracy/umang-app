package harsh.com.umangapp.dto;

import java.io.Serializable;

/**
 * Created by Harsh on 9/28/2017.
 */

public class MediaDTO implements Serializable {

    String labelImage;
    String label;
    String link;
    String mediaImage;

    public String getLabelImage() {
        return labelImage;
    }

    public void setLabelImage(String labelImage) {
        this.labelImage = labelImage;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getMediaImage() {
        return mediaImage;
    }

    public void setMediaImage(String mediaImage) {
        this.mediaImage = mediaImage;
    }
}
