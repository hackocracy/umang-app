package harsh.com.umangapp.dto;

import java.io.Serializable;

/**
 * Created by Harsh on 9/28/2017.
 */

public class GalleryDTO implements Serializable {

    String filepath;
    String filename;

    public String getFilepath() {
        return filepath;
    }

    public void setFilepath(String filepath) {
        this.filepath = filepath;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }
}
