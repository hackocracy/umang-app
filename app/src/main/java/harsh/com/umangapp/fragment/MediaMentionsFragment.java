package harsh.com.umangapp.fragment;


import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import harsh.com.umangapp.R;
import harsh.com.umangapp.adapter.MediaAdapter;
import harsh.com.umangapp.app.AppController;
import harsh.com.umangapp.constant.NetworkConstants;
import harsh.com.umangapp.dto.MediaDTO;
import harsh.com.umangapp.util.NetworkCheck;

/**
 * A simple {@link Fragment} subclass.
 */
public class MediaMentionsFragment extends Fragment {

    @Bind(R.id.mediaMentions)
    RecyclerView mediaMentions;
    @Bind(R.id.swipeContainerMedia)
    SwipeRefreshLayout swipeRefreshMedia;
    View parentView;
    private List<MediaDTO> mediaDTOList;
    private MediaAdapter adapter;

    public MediaMentionsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        parentView = inflater.inflate(R.layout.fragment_media_mentions, container, false);
        ButterKnife.bind(this, parentView);
        populate();
        return parentView;
    }

    private void populate() {
        swipeRefreshMedia.setColorSchemeResources(R.color.colorPrimary,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

        swipeRefreshMedia.post(
                new Runnable() {
                    @Override
                    public void run() {
                        getData();
                    }
                }
        );
        swipeRefreshMedia.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getData();
            }
        });
    }

    private void getData() {
        swipeRefreshMedia.setRefreshing(true);
        if (!NetworkCheck.isNetworkAvailable(getActivity())) {
            Snackbar.make(swipeRefreshMedia, "No Internet Connection!!!", 2000).show();
            swipeRefreshMedia.setRefreshing(false);
            return;
        }
        mediaDTOList = new ArrayList<>();
        StringRequest request = new StringRequest(Request.Method.GET, NetworkConstants.FEED_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.i("Response", response);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray jsonArray = jsonObject.getJSONArray("feed");
                    if (jsonArray.length() > 0) {
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                            MediaDTO dto = new MediaDTO();
                            dto.setLabelImage(jsonObject1.getString("profilePic"));
                            dto.setLabel(jsonObject1.getString("name"));
                            dto.setLink(jsonObject1.getString("url"));
                            dto.setMediaImage(jsonObject1.getString("image"));
                            mediaDTOList.add(dto);
                        }
                    } else {
                        Toast.makeText(getActivity(), "Some Error Occurred!!!", Toast.LENGTH_SHORT).show();
                    }
                    if (getActivity() != null) {
                        adapter = new MediaAdapter(getActivity(), mediaDTOList, new MediaAdapter.OnItemClickListener() {
                            @Override
                            public void onItemClick(MediaDTO mediaDTO) {
                                if (mediaDTO.getLink() == null) {
                                    Snackbar.make(swipeRefreshMedia, "No Link Provided!!!", 2000).show();
                                    return;
                                }
                                try {
                                    Uri uri = Uri.parse(mediaDTO.getLink());
                                    Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                                    startActivity(intent);
                                } catch (ActivityNotFoundException e) {
                                    Snackbar.make(swipeRefreshMedia, "No Link Provided!!!", 2000).show();
                                }
                            }
                        });
                    }
                    mediaMentions.setAdapter(adapter);
                    mediaMentions.setLayoutManager(new LinearLayoutManager(getActivity()));
                    mediaMentions.setItemAnimator(new DefaultItemAnimator());
                    swipeRefreshMedia.setRefreshing(false);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Error::", error.toString());
                Toast.makeText(getActivity(), "Some Error Occurred!!!", Toast.LENGTH_SHORT).show();
            }
        });
        AppController.getInstance().addToRequestQueue(request);
    }
}
