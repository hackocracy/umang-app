package harsh.com.umangapp.fragment;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;
import harsh.com.umangapp.R;
import harsh.com.umangapp.activity.EventActivity;
import harsh.com.umangapp.adapter.EventsAdapter;
import harsh.com.umangapp.app.AppController;
import harsh.com.umangapp.constant.NetworkConstants;
import harsh.com.umangapp.dto.EventDTO;
import harsh.com.umangapp.util.NetworkCheck;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment {

    @Bind(R.id.swipeEvents)
    SwipeRefreshLayout swipeEvents;
    @Bind(R.id.listEvents)
    RecyclerView listEvents;
    View parentView;
    private List<EventDTO> eventDTOList;
    private EventsAdapter adapter;
    private SharedPreferences preferences;

    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        parentView = inflater.inflate(R.layout.fragment_home, container, false);
        ButterKnife.bind(this, parentView);
        preferences = getActivity().getSharedPreferences("AndroidLogin", Context.MODE_PRIVATE);
        populate();
        return parentView;
    }

    private void populate() {
        swipeEvents.setColorSchemeResources(R.color.colorPrimary,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

        swipeEvents.post(
                new Runnable() {
                    @Override
                    public void run() {
                        getData();
                    }
                }
        );
        swipeEvents.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getData();
            }
        });
    }

    private void getData() {
        swipeEvents.setRefreshing(true);
        if (!NetworkCheck.isNetworkAvailable(getActivity())) {
            Snackbar.make(swipeEvents, "No Internet Connection!!!", 2000).show();
            swipeEvents.setRefreshing(false);
            return;
        }
        eventDTOList = new ArrayList<>();
        StringRequest request = new StringRequest(Request.Method.POST, NetworkConstants.LOGIN_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.i("Response", response);
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    if (jsonArray.length() > 0) {
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                            EventDTO dto = new EventDTO();
                            dto.setEventId(jsonObject1.getString("eventid"));
                            dto.setEventImageUrl(NetworkConstants.EVENT_IMAGE_URL + jsonObject1.getString("img"));
                            dto.setEventName(jsonObject1.getString("name"));
                            dto.setEventDesc(jsonObject1.getString("description"));
                            dto.setStartDate(jsonObject1.getString("start"));
                            dto.setEndDate(jsonObject1.getString("stop"));
                            dto.setVenue(jsonObject1.getString("venue"));
                            dto.setCode(jsonObject1.getString("myevent"));
                            eventDTOList.add(dto);
                        }
                    } else {
                        Toast.makeText(getActivity(), "Some Error Occurred!!!", Toast.LENGTH_SHORT).show();
                    }
                    if (getActivity() != null) {
                        adapter = new EventsAdapter(getActivity(), eventDTOList, new EventsAdapter.OnItemClickListener() {
                            @Override
                            public void onItemClick(EventDTO eventDTO) {
                                Bundle bundle = new Bundle();
                                bundle.putSerializable("event", eventDTO);
                                Intent intent = new Intent(getActivity(), EventActivity.class);
                                intent.putExtra("data", bundle);
                                startActivity(intent);
                            }
                        });
                    }
                    listEvents.setAdapter(adapter);
                    listEvents.setLayoutManager(new GridLayoutManager(getActivity(), 2));
                    listEvents.setItemAnimator(new DefaultItemAnimator());
                    swipeEvents.setRefreshing(false);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Error::", error.toString());
                Toast.makeText(getActivity(), "Some Error Occurred!!!", Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("getevent", "getevent");
                params.put("emailid", preferences.getString("user_email", ""));
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(request);
    }
}
