package harsh.com.umangapp.fragment;


import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import harsh.com.umangapp.R;
import harsh.com.umangapp.adapter.GalleryAdapter;
import harsh.com.umangapp.dto.GalleryDTO;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 */
public class PhotoGalleryFragment extends Fragment {

    private static final int CAMERA_REQUEST = 1777;
    @Bind(R.id.photoGallery)
    RecyclerView photoGallery;
    @Bind(R.id.fabCamera)
    FloatingActionButton fabCamera;
    View parentView;
    File sdRoot;
    String dir;
    String fileName;
    private List<GalleryDTO> galleryDTOList = new ArrayList<>();
    private GalleryAdapter adapter;
    private File[] listFile;
    private String[] FilePathStrings;
    private String[] FileNameStrings;

    public PhotoGalleryFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        parentView = inflater.inflate(R.layout.fragment_photo_gallery, container, false);
        ButterKnife.bind(this, parentView);
        populate();
        return parentView;
    }

    private void populate() {
        fabCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkPermissions();
                Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                sdRoot = Environment.getExternalStorageDirectory();
                dir = "/DCIM/Umang/";
                Date date = new Date();
                DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
                fileName = dateFormat.format(date) + ".jpg";
                final File mkdir = new File(sdRoot, dir);
                if (!mkdir.exists()) {
                    mkdir.mkdirs();
                }
                final File file = new File(sdRoot, dir + fileName);
                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file));
                Uri contentUri = Uri.fromFile(file);
                Intent mediaIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
                mediaIntent.setData(contentUri);
                getActivity().sendBroadcast(mediaIntent);
                startActivityForResult(cameraIntent, CAMERA_REQUEST);
                adapter.notifyDataSetChanged();
            }
        });

        if (!Environment.getExternalStorageState().equals(
                Environment.MEDIA_MOUNTED)) {
            Toast.makeText(getActivity(), "Error! No SDCARD Found!", Toast.LENGTH_LONG)
                    .show();
        } else {
            // Locate the image folder in your SD Card
            sdRoot = new File(Environment.getExternalStorageDirectory()
                    + File.separator + "DCIM/Umang");
            // Create a new folder if no folder named SDImageTutorial exist
            sdRoot.mkdirs();
        }

        if (sdRoot.isDirectory()) {
            listFile = sdRoot.listFiles();
            // Create a String array for FilePathStrings
            FilePathStrings = new String[listFile.length];
            // Create a String array for FileNameStrings
            FileNameStrings = new String[listFile.length];

            for (int i = 0; i < listFile.length; i++) {
                GalleryDTO dto = new GalleryDTO();
                // Get the path of the image file
                dto.setFilepath(listFile[i].getAbsolutePath());
                // Get the name image file
                dto.setFilename(listFile[i].getName());
                galleryDTOList.add(dto);
            }

            adapter = new GalleryAdapter(getActivity(), galleryDTOList, new GalleryAdapter.OnItemClickListener() {
                @Override
                public void onItemClick(GalleryDTO galleryDTO) {
                    Toast.makeText(getActivity(), "Clicked on " + galleryDTO.getFilename(), Toast.LENGTH_SHORT).show();
                }
            });
            photoGallery.setAdapter(adapter);
            photoGallery.setLayoutManager(new GridLayoutManager(getActivity(), 2));
            photoGallery.setItemAnimator(new DefaultItemAnimator());
            adapter.notifyDataSetChanged();
        }
    }

    private void checkPermissions() {
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA)
                == PackageManager.PERMISSION_DENIED && ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_DENIED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 1) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {

            } else {
                Toast.makeText(getActivity(), "camera permission denied", Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case CAMERA_REQUEST:
                if (resultCode == RESULT_OK) {
                    File file = new File(sdRoot, dir + fileName);
                    /*Bitmap photo = decodeSampledBitmapFromFile(file.getAbsolutePath(), 3264, 3264);
                    GalleryDTO dto = new GalleryDTO();*/
                }
                break;
        }
    }
}
