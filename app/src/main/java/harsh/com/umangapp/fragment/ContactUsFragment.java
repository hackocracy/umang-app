package harsh.com.umangapp.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.vansuita.materialabout.builder.AboutBuilder;
import com.vansuita.materialabout.views.AboutView;

import harsh.com.umangapp.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class ContactUsFragment extends Fragment {


    public ContactUsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment


        return inflater.inflate(R.layout.fragment_contact_us, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setRetainInstance(true);

        final FrameLayout flHolder = getActivity().findViewById(R.id.about);

        AboutBuilder builder = AboutBuilder.with(getActivity())
                .setAppIcon(R.mipmap.ic_app_icon)
                .setAppName(R.string.app_name)
                .setPhoto(R.drawable.boy)
                .setCover(R.mipmap.profile_cover)
                .setLinksAnimated(true)
                .setDividerDashGap(13)
                .setName("Spidey")
                .setSubTitle("Organisation")
                .setLinksColumnsCount(4)
                .setBrief("We are spiders!!!")
                .addGooglePlayStoreLink("")
                .addGitHubLink("Harsh18894")
                .addBitbucketLink("Harsh_Deep")
                .addFacebookLink("harsh18894")
                .addTwitterLink("harsh_singh21")
                .addInstagramLink("harsh18894")
                .addGooglePlusLink("+harshdeep18894")
                .addYoutubeChannelLink("")
                .addDribbbleLink("")
                .addLinkedInLink("harsh-deep-singh-1b4387113/")
                .addEmailLink("harshdeep18894@gmail.com")
                .addWhatsappLink("Harsh", "+917895052263")
                .addSkypeLink("harsh.singh50")
                .addGoogleLink("user")
                .addAndroidLink("user")
                .addWebsiteLink("https://www.google.com/")
                .addFiveStarsAction()
                .addMoreFromMeAction("")
                .setVersionNameAsAppSubTitle()
                .addShareAction(R.string.app_name)
                .addUpdateAction()
                .setActionsColumnsCount(2)
                .addFeedbackAction("harshdeep18894@gmail.com")
                .addPrivacyPolicyAction("")
                .addIntroduceAction((Intent) null)
                .addHelpAction((Intent) null)
                .addChangeLogAction((Intent) null)
                .addRemoveAdsAction((Intent) null)
                .addDonateAction((Intent) null)
                .setWrapScrollView(true)
                .setShowAsCard(true);


        AboutView view = builder.build();

        flHolder.addView(view);
    }
}
